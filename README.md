# Wilson Mintilana

## About me:
Prior to AND, I worked mainly in the Finance Industry. Started at S&P and then on to Morningstar. Several roles from creating market feeds, to analyst and then developer.
The job that thaught me the most about the importance of communication, was when I worked at SwissAir.
I love IT, though I focus a lot on .Net and back end, I'm well rounded also on front end.
I love programming (.Net), though I'm not averse at getting out of my comforzone

![Build Status](https://img.shields.io/badge/Build_Level-Importance_of_team_work-brightgreen.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Using_lateral_thinking_for_alternative_ways_to_achieve_desirable_result-brightgreen.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Importance_of_communication_with_collegues_and_non_colleagues-brightgreen.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Past_experiences_do_not_represent_me,_they_prepare_me_to_what_I'm_about_to_be-brightgreen.svg)


## Religious beliefs:
Conversation is an art-form.

Snowboarding is the pathway to heaven.
![Build Status](https://img.shields.io/badge/Build_Level-Agnostic_and_known_to_pray_to_mountain_gods_regularly-green.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Opinionated,_known_to_start_discussions_in_an_empty_room-green.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Coffee_is_coffee.Nescafe_and_other_soluble_pigswill,_do_not_qualify-green.svg)

## What I'm really good at
Life is a journey. I feel there is always something to improve on what I know.
I love technology to the point of being obsessive in tasks I take on. I see it as a puzzle that I need to solve and understand, making it a personal journey.

![Build Status](https://img.shields.io/badge/Build_Level-Self_centered,_but_I_still_play_very_well_with_others-blue.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Increased_confidence_in_working_in_previously_unknown_tech_with_no_prep-brightgreen.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Realising_that_"blockers"_are_not_necessarily_a_bad_sign_for_the_client-green.svg)


## How I work:
Here is an example of my tasks whilst at Whitbread. The Authoring department were a unable to identify errors in [AEM](https://www.adobe.com/marketing/experience-manager.html) templates when synching with [AdobeCampaign](https://www.adobe.io/apis/experiencecloud/campaign.html)
The below video outlines the steps taken to diagnose and identify the errors using lateral thinking, as the solution was not provided by Adobe.

https://bitbucket.org/mestreOfico/bonusmission/raw/e78662569df156f1d3b2143a65453bf6c46832f9/media/Mission_AEM.mov

## Outcome

![Build Status](https://img.shields.io/badge/Build_Level-Drawing_on_past_experiences_and_alernative_resources_to_optimal_effect-brightgreen.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Trying_be_not_so_focused_on_the_tasks-yellow.svg)

![Build Status](https://img.shields.io/badge/Build_Level-Take_pride_in_my_stuborness.Helps_me_to_find_innovative_solutions-blue.svg)


![alt text](https://bitbucket.org/mestreOfico/bonusmission/raw/e78662569df156f1d3b2143a65453bf6c46832f9/media/awesome.jpg)


![Build Status](https://img.shields.io/badge/Build_Level-Enjoy_learning_from_others-green.svg)

![alt text](https://bitbucket.org/mestreOfico/bonusmission/raw/e78662569df156f1d3b2143a65453bf6c46832f9/media/learning_snow.jpg)


![Build Status](https://img.shields.io/badge/Build_Level-Constantly_updating_what_it_is_to:-gree.svg)

![alt text](https://bitbucket.org/mestreOfico/bonusmission/raw/e78662569df156f1d3b2143a65453bf6c46832f9/media/beWilson.jpg)

